############################################
# Import and include packages
############################################
# Import basic python packages
import os
import matplotlib.pyplot as plt
import sys
import numpy as np
import torch.autograd as autograd
import torch
import torch.nn as nn
from torch.optim import LBFGS, Adam

# Set paths to known folder structure for different PINNs and lib files.
file_path = os.path.dirname(__file__)
# Absolute path to lib folder
parent_dir = "/".join([file_path, "../../.."])
sys.path.append(parent_dir)


class Net(nn.Module):
    def __init__(self, min_values, max_values, loss_scaling):
        super(Net, self).__init__()

        self.device = torch.device(
            "cuda:0" if torch.cuda.is_available() else "cpu"
        )  # Use GPU if available
        self.number_of_nodes = 60
        self.number_of_layers = 5
        self.number_inputs_without_ids = 2

        self.layer_first = nn.Linear(
            self.number_inputs_without_ids, self.number_of_nodes
        )
        self.layer = [
            nn.Linear(self.number_of_nodes, self.number_of_nodes).to(self.device)
            for i in range(self.number_of_layers - 2)
        ]
        self.layer_last = nn.Linear(self.number_of_nodes, 1)

        # self.activation = nn.LeakyReLU()  # Activation function
        self.activation = nn.Tanh()  # Activation function
        self.loss = nn.HuberLoss()  # Loss function
        self.min_values = min_values.to(self.device)
        self.max_values = max_values.to(self.device)
        self.loss_scaling = loss_scaling
        self.iteration = 0

        # physical constants
        self.diff_coeff                = 1/30
        self.ambient_Temperature       = 0
        self.ambient_Temperature_Top   = 1


    def forward(self, x):
        # scale x columns between -1 and 1
        y = 2.0 * (x - self.min_values) / (self.max_values - self.min_values) - 1.0

        y = self.activation(self.layer_first(y))
        for i in range(self.number_of_layers - 2):
            y = self.activation(self.layer[i](y))
        y = self.layer_last(y)

        return y

    def loss_bc1(self, input):
        """
        Dirichlet Boundary
        """
        output = self.forward(input)
        return model.loss(output[:,0], self.ambient_Temperature_Top*torch.ones(input.shape[0]))

    def loss_bc2(self, input):
        """
        Dirichlet Boundary
        """
        output = self.forward(input)
        return model.loss(output[:,0], self.ambient_Temperature*torch.ones(input.shape[0]))

    def loss_bc3(self, input):
        """
        Dirichlet Boundary
        """
        output = self.forward(input)
        return model.loss(output[:,0], self.ambient_Temperature*torch.ones(input.shape[0]))

    def loss_bc4(self, input):
        """
        Dirichlet Boundary
        """
        output = self.forward(input)
        return model.loss(output[:,0], self.ambient_Temperature*torch.ones(input.shape[0]))


def find_min_max(matrix):
    # Get the number of columns in the matrix
    num_columns = matrix.shape[1]

    # Initialize lists to store the min and max values for each column
    min_values = []
    max_values = []

    # Iterate over each column
    for col in range(num_columns):
        column_values = matrix[:, col]  # Extract the column values

        # Find the minimum and maximum values in the column
        min_val = torch.min(column_values)
        max_val = torch.max(column_values)

        if min_val == max_val:
            min_values.append(min_val - 1)
            max_values.append(max_val + 1)
        else:
            min_values.append(min_val)
            max_values.append(max_val)

    return (
        torch.tensor(min_values, dtype=torch.float32, requires_grad=True)[:2],
        torch.tensor(max_values, dtype=torch.float32, requires_grad=True)[:2],
    )


def loss_pde(inputs): # Extract inputs
    inputs.requires_grad_(True)

    # predict solution by forward pass
    # Prediction contain either: T
    predictions = model.forward(inputs)

    # Compute first derivatives of T
    dT_dX = autograd.grad(predictions[:,0], inputs, torch.ones([inputs.shape[0]]), retain_graph=True, create_graph=True)[0]
    # Compute second derivatives of T
    dTdx_dX = autograd.grad(dT_dX[:,0], inputs, torch.ones([inputs.shape[0]]), retain_graph=True, create_graph=True)[0]
    # Extract derivative with respect to x coordinate
    d2Tdx2 = dTdx_dX[:,0]
    dTdy_dX = autograd.grad(dT_dX[:,1], inputs, torch.ones([inputs.shape[0]]), retain_graph=True, create_graph=True)[0]
    # Extract derivative with respect to y coordinate
    d2Tdy2 = dTdy_dX[:,1]

    # Get derivatives of fst_der_pred
    pde_residual =  - model.diff_coeff * (d2Tdx2 + d2Tdy2)

    # Return MSE-loss of residual with respect to zero vector
    pde_loss = model.loss(pde_residual, torch.zeros(pde_residual.shape))

    return pde_loss


def compute_loss(pinn_input):
    pde_nodes = pinn_input[pinn_input[:, model.number_inputs_without_ids] == -99]

    boundary_input_1 = pinn_input[pinn_input[:, model.number_inputs_without_ids] == 1]

    boundary_input_2 = pinn_input[pinn_input[:, model.number_inputs_without_ids] == 2]

    boundary_input_3 = pinn_input[pinn_input[:, model.number_inputs_without_ids] == 3]

    boundary_input_4 = pinn_input[pinn_input[:, model.number_inputs_without_ids] == 4]

    (
        loss_boundary_1,
        loss_boundary_2,
        loss_boundary_3,
        loss_boundary_4,
        loss_pde_1,
    ) = (0, 0, 0, 0, 0)

    # Start with boundary errors
    if len(boundary_input_1) > 0:
        loss_boundary_1 = model.loss_bc1(boundary_input_1[:, : model.number_inputs_without_ids])
    if len(boundary_input_2) > 0:
        loss_boundary_2 = model.loss_bc2(boundary_input_2[:, : model.number_inputs_without_ids])
    if len(boundary_input_3) > 0:
        loss_boundary_3 = model.loss_bc3(boundary_input_3[:, : model.number_inputs_without_ids])
    if len(boundary_input_4) > 0:
        loss_boundary_4 = model.loss_bc4(boundary_input_4[:, : model.number_inputs_without_ids])
        

    # Now compute the pde loss
    if len(pde_nodes) > 0:
        loss_pde_1 = loss_pde(pde_nodes[:,: model.number_inputs_without_ids])

    # sum all losses
    loss_total = (
        model.loss_scaling[0] * loss_pde_1
        + model.loss_scaling[1] * loss_boundary_1
        + model.loss_scaling[2] * loss_boundary_2
        + model.loss_scaling[3] * loss_boundary_3
        + model.loss_scaling[4] * loss_boundary_4
    )

    loss_history.append(loss_total.item())

    print("#########################################")
    print(
        f"Current iteration: {model.iteration} \t\t and error: {loss_total.item():.2E}"
    )
    print(
        f"loss_pde: {(model.loss_scaling[0]*loss_pde_1):.2E} \
        \t loss_boundary_1: {(model.loss_scaling[1]*loss_boundary_1):.2E} \
        \t loss_boundary_2: {(model.loss_scaling[2]*loss_boundary_2):.2E} \
        \t loss_boundary_3: {(model.loss_scaling[3]*loss_boundary_3):.2E} \
        \t loss_boundary_4: {(model.loss_scaling[4]*loss_boundary_4):.2E} "
    )

    return loss_total


def closure():
    optimizer.zero_grad()
    loss = compute_loss(data)
    loss.backward()
    return loss


def shuffle_data(pinn_input):
    """
    Shuffle the data
    """
    idx = np.random.permutation(len(pinn_input))
    pinn_input = pinn_input[idx]

    return pinn_input


def generate_dataset():
    # Set number of point for the boundaries and domain
    n_boundaries = 100
    n_domain     = 1000

    # Set domain size
    x_min, y_min = 0, 0 
    x_max, y_max = 1, 1

    # generate points for the boundaries
    boundary_1 = torch.zeros((n_boundaries,3))
    boundary_1[:,0] = (x_max-x_min) * torch.rand(n_boundaries)
    boundary_1[:,1] = 1.0
    boundary_1[:,2] = 1.0

    boundary_2 = torch.zeros((n_boundaries,3))
    boundary_2[:,0] = 0.0
    boundary_2[:,1] = (y_max-y_min) * torch.rand(n_boundaries)
    boundary_2[:,2] = 2.0

    boundary_3 = torch.zeros((n_boundaries,3))
    boundary_3[:,0] = (x_max-x_min) * torch.rand(n_boundaries)
    boundary_3[:,1] = 0.0
    boundary_3[:,2] = 3.0

    boundary_4 = torch.zeros((n_boundaries,3))
    boundary_4[:,0] = 1.0
    boundary_4[:,1] = (y_max-y_min) * torch.rand(n_boundaries)
    boundary_4[:,2] = 4.0

    domain = torch.zeros((n_domain,3))
    domain[:,0] = (x_max-x_min) * torch.rand(n_domain)
    domain[:,1] = (y_max-y_min) * torch.rand(n_domain)
    domain[:,2] = -99.0

    full_data = torch.cat((boundary_1, boundary_2, boundary_3, boundary_4, domain))

    return full_data


if __name__ == "__main__":
    # set seed for torch and numpy
    torch.manual_seed(1234)
    np.random.seed(1234)

    # load trainings data
    data = generate_dataset()    

    # plt.scatter(data[:,0], data[:,1])
    # plt.show()

    # shuffle data
    pinn_input = shuffle_data(data)

    # scale inputs, find the max and min values in pinn_input for each row
    min_values, max_values = find_min_max(pinn_input)

    # Define the loss scaling first is for pde rest goes put for boundaries
    loss_scaling = torch.tensor(
        [5.0, 20.0, 10.0, 10.0, 10.0],
        dtype=torch.float32,
        requires_grad=True,
    )

    # create NN
    # input structure is [x, y, t, T_cylinder, u_max, u, v]
    # output structure is [T]
    model = Net(min_values, max_values, loss_scaling)
    model.to(model.device)
    data = torch.tensor(data, dtype=torch.float32).to(model.device)

    print("#########################################")
    print("PINN Input shape: ", pinn_input.shape)
    print("#########################################")

    # Plotting loss
    loss_history = []

    # Train adam
    optimizer = Adam(model.parameters(), lr=0.1)
    print("Starting ADAM optimizer")
    try:
        for i in range(200):
            model.iteration += 1

            optimizer.zero_grad()
            loss = compute_loss(data)
            loss_history.append(loss.item())
            loss.backward()
            optimizer.step()

    except KeyboardInterrupt:
        pass

    # Train BFGS
    optimizer = LBFGS(
        model.parameters(),
        lr=1.0,
        max_iter=20,
        max_eval=30,
        tolerance_grad=1e-5,
        tolerance_change=1e-9,
        line_search_fn="strong_wolfe",
    )
    print(
        f"Starting BFGS optimizer with learning rate {optimizer.param_groups[0]['lr']}"
    )
    try:
        for i in range(100):
            model.iteration += 1
            optimizer.step(closure)
    except KeyboardInterrupt:
        pass

    x = np.linspace(0.0,1.0,100)
    y = np.linspace(0.0, 1.0, 100)
    X,Y = np.meshgrid(x,y)

    Z = np.zeros((100, 100))
    for i_x, i in enumerate(x):
        for j_y, j in enumerate(y):
            Z[j_y, i_x] = model.forward(torch.tensor([[i, j]], dtype=torch.float32))

    fig, (ax1,ax2) = plt.subplots(2,1)
    cp = ax1.contourf(X, Y, Z,levels=100)
    fig.colorbar(cp)
    ax1.set_title('Temperature')

    # plot loss_history with log y axis
    ax2.semilogy(loss_history)
    ax2.set_title("Loss history")

    plt.tight_layout()
    plt.show()
