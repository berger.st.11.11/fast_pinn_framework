############################################
# Import and include packages
############################################
# Import basic python packages
import os
from turtle import up, update
import matplotlib.pyplot as plt
import sys
import numpy as np
import torch.autograd as autograd
import torch
import torch.nn as nn
from torch.optim import LBFGS, Adam

# Set paths to known folder structure for different PINNs and lib files.
file_path = os.path.dirname(__file__)
# Absolute path to lib folder
parent_dir = "/".join([file_path, "../../.."])
sys.path.append(parent_dir)


class Net(nn.Module):
    def __init__(self, min_values, max_values, loss_scaling):
        super(Net, self).__init__()

        self.device = torch.device(
            "cuda:0" if torch.cuda.is_available() else "cpu"
        )  # Use GPU if available
        self.number_of_nodes = 80
        self.number_of_layers = 10
        self.number_inputs_without_ids = 2

        self.layer_first = nn.Linear(
            self.number_inputs_without_ids, self.number_of_nodes
        )
        self.layer = [
            nn.Linear(self.number_of_nodes, self.number_of_nodes).to(self.device)
            for i in range(self.number_of_layers - 2)
        ]
        self.layer_last = nn.Linear(self.number_of_nodes, 1)

        # self.activation = nn.LeakyReLU()  # Activation function
        self.activation = nn.Tanh()  # Activation function
        self.loss = nn.HuberLoss()  # Loss function
        self.min_values = min_values.to(self.device)
        self.max_values = max_values.to(self.device)
        self.loss_scaling = loss_scaling
        self.iteration = 0

        # physical constants
        self.diff_coeff                = 0.49
        self.heat_transfer_coefficient = 26
        self.ambient_Temperature       = 300
        self.adv_x                     = 0.002

    def phi_1(self, x):
        """
        Distance function to the inflow boundary.
        """
        return abs(x[:,0])
        # return abs(x[:,0] * (x[:,1]-1.25e-2))

    def phi_2(self, x):
        """
        Distance function to the lower Robin boundary.
        """
        return abs(abs(x[:,1]) - 1.25e-02)         

        # lower_boundary = x[:,1] + 1.25e-02
        # upper_boundary = x[:,1] - 1.25e-02
        # return lower_boundary*upper_boundary/(lower_boundary + upper_boundary)
        # return 1/torch.sqrt(1/lower_boundary**4 + 1/upper_boundary**4)
        # return abs(x[:,1] + 1.25e-02)

    def w_1(self, x):
        """
        First weighting function
        """
        phi_1 = self.phi_1(x)
        phi_2 = self.phi_2(x)

        result = phi_1/(phi_1 + phi_2**2) 
        # temporary fix for the lower left corner, since both phi are 0 there
        result[result.isnan()] = 1
        result[result.isinf()] = 1
        
        return result

    def w_2(self, x):
        """
        Second weighting function
        """
        phi_1 = self.phi_1(x)
        phi_2 = self.phi_2(x)
        
        result = phi_2**2/(phi_1 + phi_2**2) 
        # temporary fix for the lower left corner, since both phi are 0 there
        result[result.isnan()] = 0
        result[result.isinf()] = 0
        
        return result

    def u_1(self, x):
        """
        First polynomial coefficient
        """
        outputs_ones = torch.ones(x.shape[0])

        return 473.0*outputs_ones

    def u_2(self, x):
        """
        Second polynomial coefficient
        """
        x.requires_grad_(True)
        # Do a manual pass through the PINN
        y= 2.0 * (x - self.min_values) / (self.max_values - self.min_values) - 1.0

        y = self.activation(self.layer_first(y))
        for i in range(self.number_of_layers - 2):
            y = self.activation(self.layer[i](y))
        output = self.layer_last(y)

        # Compute the gradients of the prediction
        dT_dX = autograd.grad(output[:,0], x, torch.ones([x.shape[0]]), retain_graph=True, create_graph=True)[0]
        dT_dx = dT_dX[:,0]
        dT_dy = dT_dX[:,1]

        # Compute the gradient of the distance function to the Robin BC
        phi_2 = self.phi_2(x)
        dphi_2_dX = autograd.grad(phi_2, x, torch.ones([x.shape[0]]), retain_graph=True, create_graph=True)[0]
        dphi_2_dx = dphi_2_dX[:,0]
        dphi_2_dy = dphi_2_dX[:,1]

        # Compute the D_1_phi_2 gradient (31b)
        tmp_1 = -(dphi_2_dx * dT_dx + dphi_2_dy * dT_dy)

        return (output[:,0] + phi_2 *(self.heat_transfer_coefficient*output[:,0] + tmp_1))\
            - phi_2 * self.heat_transfer_coefficient* self.ambient_Temperature

    def forward(self, x):
        # scale x columns between -1 and 1
        y = 2.0 * (x - self.min_values) / (self.max_values - self.min_values) - 1.0

        y = self.activation(self.layer_first(y))
        for i in range(self.number_of_layers - 2):
            y = self.activation(self.layer[i](y))
        y = self.layer_last(y)

        w_1   = self.w_1(x)
        w_2   = self.w_2(x)
        u_1   = self.u_1(x)
        u_2   = self.u_2(x)
        phi_1 = self.phi_1(x)
        phi_2 = self.phi_2(x)
        # Return polynomial constructed like (35a)
        ansatz = w_1*u_2 + w_2*u_1 + (phi_1*phi_2**2)*y[:,0]
        # ansatz = w_2*u_1 + (phi_1*phi_2**2)*y[:,0]

        ansatz = ansatz.view(ansatz.shape[0],1)

        return torch.concat((ansatz, y[:,1:]), axis=1)

    def loss_bc1(self, input):
        output = self.forward(input)
        return model.loss(output[:,0], 473*torch.ones(input.shape[0]))

    def loss_bc2(self, input):
        input.requires_grad_(True)
        predictions = self.forward(input)
        # Compute first derivatives of T
        dT_dX = autograd.grad(predictions[:,0], input, torch.ones([input.shape[0]]), retain_graph=True, create_graph=True)[0]
        # Set torch vector with ones:
        outputs_zeros = torch.zeros(predictions.shape[0])

        res_robin_low = self.heat_transfer_coefficient*(predictions[:,0] - self.ambient_Temperature) - dT_dX[:,1]
        loss_robin_low = self.loss(res_robin_low, outputs_zeros)
        return loss_robin_low

    def loss_bc3(self, input):
        loc_inputs = input
        loc_inputs.requires_grad_(True)

        # Compute predictions
        predictions = self.forward(loc_inputs)
        # Compute first derivatives of T
        dT_dX = autograd.grad(predictions[:,0], input, torch.ones([input.shape[0]]), retain_graph=True, create_graph=True)[0]
        # Set torch vector with ones:
        outputs_zero = torch.zeros(predictions.shape[0])
        # Compute derivative
        loss_outlet = self.loss(dT_dX[:,0], outputs_zero[:])

        return loss_outlet

    def loss_bc4(self, inputs):
        loc_inputs = inputs
        loc_inputs.requires_grad_(True)

        # Compute predictions
        predictions = self.forward(loc_inputs)
        # Compute first derivatives of T
        dT_dX = autograd.grad(predictions[:,0], inputs, torch.ones([inputs.shape[0]]), retain_graph=True, create_graph=True)[0]

        # Set torch vector with ones:
        outputs_zero = torch.ones(predictions.shape[0])

        # Compute residual of bc
        res_robin_top = self.heat_transfer_coefficient*(predictions[:,0] - self.ambient_Temperature) - dT_dX[:,1]
        loss_robin_top = self.loss(res_robin_top, outputs_zero)

        return loss_robin_top


def find_min_max(matrix):
    # Get the number of columns in the matrix
    num_columns = matrix.shape[1]

    # Initialize lists to store the min and max values for each column
    min_values = []
    max_values = []

    # Iterate over each column
    for col in range(num_columns):
        column_values = matrix[:, col]  # Extract the column values

        # Find the minimum and maximum values in the column
        min_val = np.min(column_values)
        max_val = np.max(column_values)

        if min_val == max_val:
            min_values.append(min_val - 1)
            max_values.append(max_val + 1)
        else:
            min_values.append(min_val)
            max_values.append(max_val)

    return (
        torch.tensor(min_values, dtype=torch.float32, requires_grad=True)[:2],
        torch.tensor(max_values, dtype=torch.float32, requires_grad=True)[:2],
    )


def loss_pde(inputs):
    # Extract inputs
    inputs.requires_grad_(True)

    # predict solution by forward pass
    # Prediction contain either: T
    predictions = model.forward(inputs)

    # Compute first derivatives of T
    dT_dX = autograd.grad(predictions[:,0], inputs, torch.ones([inputs.shape[0]]), retain_graph=True, create_graph=True)[0]
    # Compute second derivatives of T
    dTdx_dX = autograd.grad(dT_dX[:,0], inputs, torch.ones([inputs.shape[0]]), retain_graph=True, create_graph=True)[0]
    # Extract derivative with respect to x coordinate
    d2Tdx2 = dTdx_dX[:,0]
    dTdy_dX = autograd.grad(dT_dX[:,1], inputs, torch.ones([inputs.shape[0]]), retain_graph=True, create_graph=True)[0]
    # Extract derivative with respect to y coordinate
    d2Tdy2 = dTdy_dX[:,1]

    # Get derivatives of fst_der_pred
    pde_residual =  - model.diff_coeff * (d2Tdx2 + d2Tdy2) + model.adv_x * dT_dX[:,0]

    # Return MSE-loss of residual with respect to zero vector
    pde_loss = model.loss(pde_residual, torch.zeros(pde_residual.shape))

    return pde_loss


def compute_loss(pinn_input):
    pde_nodes = pinn_input[pinn_input[:, model.number_inputs_without_ids] == -99]

    boundary_input_1 = pinn_input[pinn_input[:, model.number_inputs_without_ids] == 0]

    boundary_input_2 = pinn_input[pinn_input[:, model.number_inputs_without_ids] == 1]

    boundary_input_3 = pinn_input[pinn_input[:, model.number_inputs_without_ids] == 2]

    boundary_input_4 = pinn_input[pinn_input[:, model.number_inputs_without_ids] == 3]


    (
        loss_boundary_1,
        loss_boundary_2,
        loss_boundary_3,
        loss_boundary_4,
        loss_pde_1,
    ) = (0, 0, 0, 0, 0)
    # Start with boundary errors
    if len(boundary_input_1) > 0:
        loss_boundary_1 = model.loss_bc1(boundary_input_1[:, : model.number_inputs_without_ids])
    if len(boundary_input_2) > 0:
        loss_boundary_2 = model.loss_bc2(boundary_input_2[:, : model.number_inputs_without_ids])
    if len(boundary_input_3) > 0:
        loss_boundary_3 = model.loss_bc3(boundary_input_3[:, : model.number_inputs_without_ids])
    if len(boundary_input_4) > 0:
        loss_boundary_4 = model.loss_bc4(boundary_input_4[:, : model.number_inputs_without_ids])
        

    # Now compute the pde loss
    if len(pde_nodes) > 0:
        loss_pde_1 = loss_pde(pde_nodes[:,: model.number_inputs_without_ids])

    # loss_pde = 0.0
    # sum all losses
    loss_total = (
        model.loss_scaling[0] * loss_pde_1
        # + model.loss_scaling[1] * loss_boundary_1
        # + model.loss_scaling[2] * loss_boundary_2
        # + model.loss_scaling[3] * loss_boundary_3
        # + model.loss_scaling[4] * loss_boundary_4
    )

    loss_history.append(loss_total.item())

    print("#########################################")
    print(
        f"Current iteration: {model.iteration} \t\t and error: {loss_total.item():.2E}"
    )
    print(
        f"loss_pde: {(model.loss_scaling[0]*loss_pde_1):.2E} \t loss_boundary_1: {(model.loss_scaling[1]*loss_boundary_1):.2E} \t loss_boundary_2: {(model.loss_scaling[2]*loss_boundary_2):.2E} \t loss_boundary_3: {(model.loss_scaling[3]*loss_boundary_3):.2E} \t loss_boundary_4: {(model.loss_scaling[4]*loss_boundary_4):.2E} "
    )

    return loss_total


def closure():
    optimizer.zero_grad()
    loss = compute_loss(data)
    loss.backward()
    return loss

"""
def generate_batches(pinn_input, pinn_output, n_batches):
    # create batches from dataset
    batches_in, batches_out = [], []
    batch_size = int(len(pinn_input) / n_batches)

    for i in range(n_batches):
        batches_in.append(pinn_input[i * batch_size : (i + 1) * batch_size, :])
        batches_out.append(pinn_output[i * batch_size : (i + 1) * batch_size, :])

    # add the last batch
    if n_batches * batch_size < len(pinn_input):
        batches_in.append(pinn_input[n_batches * batch_size :, :])
        batches_out.append(pinn_input[n_batches * batch_size :, :])

    return batches_in, batches_out
"""

def shuffle_data(pinn_input):
    """
    Shuffle the data
    """
    idx = np.random.permutation(len(pinn_input))
    pinn_input = pinn_input[idx]

    return pinn_input


def plot_lines():
    fig, ax = plt.subplots(1,3)

    y = np.linspace(-0.0125,0.0125,50)
    np_inputs = np.ones((len(y),2))

    # Line one
    # Forward and compute first line
    np_inputs[:,0] = 0.1
    np_inputs[:,1] = y
    inputs = torch.from_numpy(np_inputs).float()
    temp = model.forward(inputs)[:,0].detach().numpy()
    ax[0].plot(temp,y)
    ax[0].title.set_text('x=0.1 [m]')
    ax[0].spines["right"].set_visible(False)
    ax[0].spines["top"].set_visible(False)

    # Line two
    np_inputs[:,0] = 0.25
    inputs = torch.from_numpy(np_inputs).float()
    temp = model.forward(inputs)[:,0].detach().numpy()
    ax[1].plot(temp,y)
    ax[1].title.set_text('x=0.25 [m]')
    ax[1].spines["left"].set_visible(False)
    ax[1].set_yticks([])
    ax[1].spines["right"].set_visible(False)
    ax[1].spines["top"].set_visible(False)

    # Line three
    np_inputs[:,0] = 0.5
    inputs = torch.from_numpy(np_inputs).float()
    temp = model.forward(inputs)[:,0].detach().numpy()
    ax[2].plot(temp,y)
    ax[2].title.set_text('x=0.5 [m]')
    ax[2].set_yticks([])
    ax[2].spines["left"].set_visible(False)
    ax[2].spines["right"].set_visible(False)
    ax[2].spines["top"].set_visible(False)

    return ax

if __name__ == "__main__":
    # set seed for torch and numpy
    torch.manual_seed(1234)
    np.random.seed(1234)

    # load trainings data
    data = np.genfromtxt('TrainingSet.csv', delimiter=',')
    ids = np.expand_dims(data[:,0],axis=1)
    data = np.concatenate([data[:,1:], ids],axis=1)
    
    # shuffle data
    pinn_input = shuffle_data(data)

    # scale inputs, find the max and min values in pinn_input for each row
    min_values, max_values = find_min_max(pinn_input)

    # Define the loss scaling first is for pde rest goes put for boundaries
    loss_scaling = torch.tensor(
        [10.0, 10.0, 10.0, 10.0, 10.0],
        dtype=torch.float32,
        requires_grad=True,
    )

    # create NN
    # input structure is [x, y, t, T_cylinder, u_max, u, v]
    # output structure is [T]
    model = Net(min_values, max_values, loss_scaling)
    model.to(model.device)
    data = torch.tensor(data, dtype=torch.float32).to(model.device)

    print("#########################################")
    print("PINN Input shape: ", pinn_input.shape)
    print("#########################################")

    # Plotting loss
    loss_history = []

    # Train adam
    optimizer = Adam(model.parameters(), lr=0.1)
    print("Starting ADAM optimizer")
    try:
        for i in range(200):
            model.iteration += 1

            optimizer.zero_grad()
            loss = compute_loss(data)
            loss_history.append(loss.item())
            loss.backward()
            optimizer.step()

    except KeyboardInterrupt:
        pass

    # Train BFGS
    optimizer = LBFGS(
        model.parameters(),
        lr=1.0,
        max_iter=20,
        max_eval=30,
        tolerance_grad=1e-5,
        tolerance_change=1e-9,
        line_search_fn="strong_wolfe",
    )
    print(
        f"Starting BFGS optimizer with learning rate {optimizer.param_groups[0]['lr']}"
    )
    try:
        for i in range(100):
            model.iteration += 1
            optimizer.step(closure)
    except KeyboardInterrupt:
        pass

    x = np.linspace(0.0,0.5,100)
    y = np.linspace(-0.0125, 0.0125, 100)
    X,Y = np.meshgrid(x,y)

    Z = np.zeros((100, 100))
    for i_x, i in enumerate(x):
        for j_y, j in enumerate(y):
            Z[j_y, i_x] = model.forward(torch.tensor([[i, j]], dtype=torch.float32))

    fig, (ax1,ax2) = plt.subplots(2,1)
    cp = ax1.contourf(X, Y, Z,levels=100)
    fig.colorbar(cp)
    ax1.set_title('Temperature')

    # plot loss_history with log y axis
    ax2.semilogy(loss_history)
    ax2.set_title("Loss history")

    plot_lines()
    plt.tight_layout()
    plt.show()
